import cv2 
import numpy as np
import os
from Operations import maskOperation,backGround,GausFilter,Sharpen,smoothing
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
        
        
def main(fore,back,mask,locx,locy,filename,PartNo):
    
    ForeGround = cv2.imread(fore)
    Mask= cv2.imread(back)
    BackGround= cv2.imread(mask)
 
        #cv2.imwrite(fore+"Smooth Mask.jpg",Mask)
    
    if PartNo == 1 :#PART 1 OPERATİONS :
       
        MaskedForeGround = maskOperation(ForeGround,Mask) #
        #cv2.imwrite(fore+"masked"+str(PartNo)+".jpg",MaskedForeGround)
        ShopedImage = backGround(MaskedForeGround,BackGround,Mask,locx,locy)
        cv2.imwrite(os.path.join("Part1",filename),ShopedImage)
    
    if PartNo == 2 :#PART 2 OPERATİONS:
        BluredBackGround= GausFilter(BackGround,4,7)
        #cv2.imwrite(fore+"Blured.jpg",BluredBackGround)
        SharpenedForeGround = Sharpen(ForeGround,0.8,4)
        #cv2.imwrite(fore +"Sharpened.jpg",SharpenedForeGround)
        MaskedSharpenForeGround = maskOperation(SharpenedForeGround,Mask)
        #cv2.imwrite(fore+"MAsked Sharpened.JPG",MaskedSharpenForeGround)
        ShopedNewImage = backGround(MaskedSharpenForeGround,BluredBackGround,Mask,locx,locy)
        cv2.imwrite(os.path.join("Part2",filename),ShopedNewImage)
    if PartNo == 3:
        Mask = smoothing(Mask,3)
        BluredBackGround= GausFilter(BackGround,4,7)
        #cv2.imwrite(fore+"Blured.jpg",BluredBackGround)
        SharpenedForeGround = Sharpen(ForeGround,0.8,4)
        #cv2.imwrite(fore +"Sharpened.jpg",SharpenedForeGround)
        MaskedSharpenForeGround = maskOperation(SharpenedForeGround,Mask)
        #cv2.imwrite(fore+"MAsked Sharpened.JPG",MaskedSharpenForeGround)
        ShopedNewImage = backGround(MaskedSharpenForeGround,BluredBackGround,Mask,locx,locy)
        cv2.imwrite(os.path.join("Bonus",filename),ShopedNewImage)
   
filelist=[["images\im4.jpg","images\mask4.png","images\hog1.jpg",0,0,"im4.jpg"],
          ["images\im2.jpg","images\mask2.png","images\pizza.jpg",30,-10,"im2.jpg"],
          ["images\im1.jpg","images\mask1.png","images\jump.jpg",60,300,"im1.jpg"],
          ["images\im3.jpg","images\mask3.png","images\snow.jpg",170,0,"im3.jpg"],
          ["images\im5.jpg","images\mask5.png","images\lego.jpg",330,430,"im5.jpg"]]



createFolder("Part1")
createFolder("Part2")
createFolder("Bonus")

for image in filelist:
    
    #main(image[0],image[1],image[2],image[3],image[4],1,True)
    main(image[0],image[1],image[2],image[3],image[4],image[5],1)
    main(image[0],image[1],image[2],image[3],image[4],image[5],2)
    main(image[0],image[1],image[2],image[3],image[4],image[5],3)
    

cv2.waitKey()


        
