import numpy as np
import cv2


def maskOperation(ForeGround,Mask):
    
    ForeGround[np.where(Mask==0)] = 0

    return ForeGround

def backGround(foreground,background,mask,row,column):
    imagerow=0
    imagecolumn=0
    h1,w1,t1=foreground.shape
    if row <0:
        imagerow = -row
        row =0
    if column <0:
        imagecolumn = -column
        column=0
    if row+h1 > background.shape[0]:
        h1 =background.shape[0]-row
        
    if column+w1 > background.shape[1]:
        w1 =background.shape[1]-column
        
    newimage=foreground[imagerow:h1,imagecolumn:w1]
    newmask =mask[imagerow:h1,imagecolumn:w1]
    
    h,w,t=newimage.shape
    shopedimage = background
    rowi=background[row:row+h,column:column+w]

    rowi[np.where(newmask!=0)]=newimage[np.where(newmask!=0)]
   
    shopedimage[row:row+h,column:column+w]=rowi
    return shopedimage


def gauskernel(sigma,a):
    
    kernel = np.ones((a,a))
    for i in range(kernel.shape[0]):
        for j in range(kernel.shape[1]):
            kernel[i][j] = gaus(i+1,j+1,sigma)
            
    kernel = kernel/kernel.sum()
    return kernel

def GausFilter(BackGround,sigma,a):
    kernel = gauskernel(sigma,a)
    destination = cv2.filter2D(BackGround,-1,kernel)
    return destination

    
def Sharpen(Foreground,alpha,sigma):
    bluringimage = GausFilter(Foreground,sigma,7)
    sharpenimage = cv2.subtract(Foreground,bluringimage)
    sharpenimage = np.uint8(sharpenimage*alpha)
    sharpenimage = cv2.add(sharpenimage,Foreground)
    
    return sharpenimage
    
    
    
def smoothing(Mask,sigma):
    
    Smoothmask = GausFilter(Mask,sigma,3)
    
    return Smoothmask
    
    


def gaus(x,y,sigma):

    gausian = ((np.e**-(((x**2)+(y**2))/(2*(sigma**2))))/ (2*np.pi*(sigma**2)))
    
    return gausian


